// 云函数入口文件

const communicationAndAppeal = require('./communication/index')
const queryAllChat = require('./queryAllChat/index')
const queryPageWjChatRecord = require('./queryPageWjChatRecord/index')
const sendMsg = require('./sendMsg/index')

const cloud = require('wx-server-sdk')
cloud.init()


exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  const api = event.api //接口类型
  const params = event.data //所传递的数据
  let exportData = {}
  //错误码
  const responseCode = await cloud.callFunction({
    name: 'code'
  })


  if (api === 'communicationAndAppeal') { //沟通诉求 (与诉求建立链接)

    const result = await communicationAndAppeal(params)
    exportData = Object.assign({
      data: result
    }, result ? responseCode.result['00000'] : responseCode.result['00001'])


  } else if (api === 'queryAllChat') {

    const result = await queryAllChat(params)
    exportData = Object.assign({
      data: result
    }, responseCode.result['00000'])


  } else if (api === 'queryPageWjChatRecord') {

    const result = await queryPageWjChatRecord(params)
    exportData = Object.assign({
      data: result
    }, responseCode.result['00000'])

  } else if (api === 'sendMsg') {

    const result = await sendMsg(params)
    exportData = Object.assign({
      data: result
    }, result ? responseCode.result['00000'] : responseCode.result['00001'])

  }


  return exportData


}